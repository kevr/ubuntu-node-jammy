FROM ubuntu:jammy

RUN rm -f /etc/apt/apt.conf.d/docker-clean
RUN apt-get -y update
RUN apt-get -y install git
RUN apt-get -y install nodejs
